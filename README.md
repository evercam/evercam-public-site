| Name   | Evercam Public Site   |
| --- | --- |
| Owner   | [@affan00](https://github.com/affan00)   |
| Version  | 1  |
| Evercam API Version  | 1.0  |
| Licence | [AGPL](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-%28agpl-3.0%29) |

The best starting point for Evercam is http://www.evercam.io/open-source

After that, you'll want to go here: https://github.com/evercam/evercam-devops

Any questions, drop us a line: http://www.evercam.io/contact
