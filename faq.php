<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Evercam -  Apps for IP Cameras. Get more from your camera.</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="cameras, CCTV, apps, integration, recording, remote storage, sharing, api, developer platform">
    <meta name="description" content="The basics you need when adding your camera to Evercam">
    <meta name="author" content="Evercam">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/faq.css" rel="stylesheet">
    <meta property="twitter:account_id" content="4503599630778866" />
    <script type="text/javascript" src="../js/hotjar-tracking.js" async></script>
    <link rel="apple-touch-icon" sizes="57x57" href="https://dash.evercam.io/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="https://dash.evercam.io/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="https://dash.evercam.io/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="https://dash.evercam.io/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="https://dash.evercam.io/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="https://dash.evercam.io/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="https://dash.evercam.io/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="https://dash.evercam.io/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="https://dash.evercam.io/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="https://dash.evercam.io/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="https://dash.evercam.io/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="https://dash.evercam.io/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="https://dash.evercam.io/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="https://dash.evercam.io/manifest.json">
    <meta name="msapplication-TileColor" content="#dc4c3f">
    <meta name="msapplication-TileImage" content="https://dash.evercam.io/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
  </head>

  <body id="values">
  <? include 'nav.php'; ?>
  <header>
      <div class="banner">
        <div class="banner-content">
          <h1>Frequently Asked Questions</h1>
          <h2>The basics you need when adding your camera to Evercam</h2>
        </div>
      </div>
  </header>

  <div id="faq" class="alt-color">
     <main>
        <section>
        <a href="http://www.evercam.io/faq#camera-buy">WHAT CAMERA SHOULD I BUY?</a></br>
        <a href="http://www.evercam.io/faq#connect-camera">HOW DO I CONNECT MY CAMERA?</a></br>
        <a href="http://www.evercam.io/faq#snapshot-url">WHAT IS A "SNAPSHOT URL" ?</a></br>
        <a href="http://www.evercam.io/faq#port-forward">WHAT IS "PORT FORWARDING"?</a></br>
        <a href="http://www.evercam.io/faq#external-ip">HOW DO I FIND MY EXTERNAL IP ADDRESS?</a></br>
        <a href="http://www.evercam.io/faq#ports">WHAT ARE PORTS?</a></br>
        </section>
  </div>
  <div>
        <section>
         <h3 id="camera-buy">WHAT CAMERA SHOULD I BUY?</h3>
         <h2>Evercam will work with any camera that can produce a jpeg (snapshot) ... which basically means any camera. We recommend Hikvision cameras as they are widely available, have a great range of models with different features and are reasonably priced.</h2>
         <h3 id="connect-camera">HOW DO I CONNECT MY CAMERA?</h3>
         <h2>To connect your camera to Evercam, first find the "Add A Camera" form from our site (either on the homepage or from the dashboard). Next you're going to need to know the following things:
         </br>
         </br>
             1. The external IP address of your camera. </br>
             2. The port that your camera is on (http and maybe also rtsp). </br>
             3. The make/model of your camera ... or the "Snapshot URL" of your camera.</h2>
         <h3 id="snapshot-url">WHAT IS A "SNAPSHOT URL" ?</h3>
         <h2>This is how Evercam knows how to get an image from your specific Make and Model of camera. For most common cameras, if you tell us the Make & Model, we'll already know the Snapshot URL ... for others, either let us know - and we'll add it to our list, or you might be able to find it out from the manufacturer themselves or, our old friend Google.</h2>
         <h3 id="port-forward">WHAT IS "PORT FORWARDING"?</h3>
         <h2>When your camera is on your own network, it automatically gets an IP address such as 192.168.0.52 . If you are also on a computer on your local network, you can type in this address and see your camera. If you're not on your local network, this address wont work.</br>
              </br>
              To get around this, one needs to create a rule on your router that will direct someone from a specific port on your router to the IP of your camera. This is called Port Forwarding, or sometimes NAT or Network Address Traversal. Some routers have other names too, it can be confusing. There is a website <a href="http://www.portforward.com" target="_blank">www.portforward.com</a> that goes into this in more detail.</br>
              </br>
              Your task is to create port forwarding rule for both the http port (usually 80) and the rtsp port (usually 554) on your camera to the outside world.</h2>
         <h3 id="external-ip">HOW DO I FIND MY EXTERNAL IP ADDRESS?</h3>
         <h2>This is easy. If you're using the network who's external IP address you want to know (that is, the same network your camera is on), then just visit<a href="http://www.whatsmyip.org" target="_blank"> http://www.whatsmyip.org</a></h2>
         <h3 id="ports">WHAT ARE PORTS?</h3>
         <h2>Every camera has various ports that do various things. For example, the camera's web interface is usually on port 80 (this is known as the http port) and the h264 stream is usually on port 554 (this is known as the rtsp port).</br>
             </br>
              For Evercam's purposes, these are the only 2 ports you need. In fact, just the http port is usually enough.</br>
              </br>
              Where it get's tricky is when you need to setup "Port Forwarding" (see above). This effectively maps your camera's local port (80?) to a different port on your router (usually not port 80, but some number between 10 and 60000). You will only know this number if you have the details of the port forwarding configuration.</br>
              </br>
              It can be tricky to know if your ports have been correctly forwarded from the perspective of someone who is not on your network. A tool for doing this is: <a href="http://www.canyouseeme.org/" target="_blank"> http://www.canyouseeme.org/</a> </h2>

        </section>

      </main>

  </div>


  <? include 'footer.php'; ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" async></script>
  <script src="/js/bootstrap.min.js" async></script>
  <script src="/js/custom.min.js" async></script>
  <div id="back-to-top"><a href="#"><?php _e( 'Back to Top', 'minti' ) ?></a></div>
  </body>
  </html>